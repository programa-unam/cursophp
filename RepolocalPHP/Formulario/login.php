<html>
<head>
    <title>Login</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    
</head>
<style>
  div#login-column {
    background: lightgrey;
    padding: 40px 25px 20px 25px;
    color: white;
}
.text-info {
    color: #1e1e1e !important;
}
</style>
<body>
    <div id="login">
       
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" action="./procesar_login.php" method="post">
                           <h3 class="text-center text-info">Login</h3><br>
                            <?php if(isset($_GET['msg'])){ ?>
                                <div class="alerta">
                                    <div class="alert alert-danger" role="alert">
                                        <?php echo $_GET['msg']; ?>
                                    </div>
                                </div>
                            <?php } ?>                          
                            <div class="form-group">
                                <label for="num_cuenta" class="text-info">Numero de cuenta:</label><br>
                                <input type="text" name="num_cuenta" id="num_cuenta" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-info">Password:</label><br>
                                <input type="password" name="pass" id="password" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <input type="submit" name="submit" class="btn btn-info btn-md" value="Entrar">
                            </div>
                          
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</html>
