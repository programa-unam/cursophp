<?php
//1. Realizar una expresión regular que detecte emails correctos.
function is_valid_email($str)
{
    if (preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/',$str)) {
        echo "El email es correcto."; 
    }else{
        echo "El email no es correcto."; 
      }
}
is_valid_email("ingcelibelnava@gmail.com");

//2. Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.
function is_curp($string){
    if (preg_match('/^[A-J1-8_]/',$string)) {
        echo "\nEl curp es correcto."; 
    }else{
        echo "\nEl curp es no correcto."; 
      }
}
is_curp("ABCD123456EFGHIJ78");

//3. Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras.
function is_lenght($string){
    if (preg_match('/^[A-Z]{50,}$/',$string)) {
        echo "\nLa palabra es correcta."; 
    }else{
        echo "\nLa palabra no es correcta."; 
      }
}
is_lenght("ABFASDSADSADSADSADSADSADSADADSADASDADADSADSAFSDSFDSFDSSDAFGDFGREQWDEADASSDFGDFGREQWVC");


//4. Crea una funcion para escapar los simbolos especiales.

//is_lenght("ABFASDSADSADSADSADSADSADSADADSADASDADADSADSAFSDSFDSFDSSDAFGDFGREQWDEADASSDFGDFGREQWVC");
$claves = '$40 por un a g3/400';
$claves = preg_quote($claves, '/');
echo "\n".$claves; // devuelve \$40 por un g3\/400

//5. Crear una expresion regular para detectar números decimales.
function is_decimal($string){
    if (preg_match('/^[0-9]{1,3}$|^[0-9]{1,3}\.[0-9]{1,3}$/',$string)) {
        echo "\nEs un numero decimal."; 
    }else{
        echo "\nNo es un numero decimal."; 
      }
}
is_decimal("3.6");

?>