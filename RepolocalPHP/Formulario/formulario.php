<?php session_start(); 
 $autenticado = $_SESSION['userActivo']['autenticado'];
 if($autenticado == 0){
  $msg = urlencode('Nesecitas estar logueado para acceder');
  header('Location:./login.php?msg='.$msg);
  die;
 }

?>
<html>
<head>
    <title>Formularios</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        span.card-header.link-header-form {
         background: #0d6efd;
        color: white;
        }
        span.card-header.link-header-form a {
            color: #ffffff8c;
            text-decoration: none;
            padding: 5px 15px 5px 5px;
        }
        a.active {
            color: white !important;
        }
    </style>
</head>
    <body>

    <div class="container">
        <div class="card">
        <span class="card-header link-header-form">
           <a href="./info.php">Home</a>
           <a href="./formulario.php" class="active">Registrar Alumnos</a>
           <a href="./cerrar_sesion.php">Cerrar sesión</a>
        </span>
        <div class="card-body">
        <?php if(isset($_GET['msg'])){ ?>
                                <div class="alerta">
                                    <div class="alert alert-danger" role="alert">
                                        <?php echo $_GET['msg']; ?>
                                    </div>
                                </div>
                            <?php } ?>                          
        <form id="login-form" class="form" action="./procesar_formulario.php" method="post">
             
            <div class="mb-3 row">
                <label for="numerodecuenta" class="col-sm-2 col-form-label">
                    Numero de cuenta
                </label>
                <div class="col-sm-10">
                <input name="num_cuenta" placeholder="Numero de cuenta"  type="text" class="form-control" id="numerodecuenta" required>
                </div>
            </div>

            <div class="mb-3 row">
                <label for="nombre" class="col-sm-2 col-form-label">
                    Nombre
                </label>
                <div class="col-sm-10">
                <input name="nombre" placeholder="Nombre"  type="text" class="form-control" id="Nombre" required>
                </div>
            
            </div>

            <div class="mb-3 row">
                <label for="primerapellido" class="col-sm-2 col-form-label">
                    Primer Apellido
                </label>
                <div class="col-sm-10">
                <input name="primer_apellido" placeholder="Primer Apellido"  type="text" class="form-control" id="primerapellido">
                </div>
            </div>

             <div class="mb-3 row">
                <label for="segundoapellido" class="col-sm-2 col-form-label">
                    Segundo Apellido
                </label>
                <div class="col-sm-10">
                <input name="segundo_apellido" placeholder="Segundo Apellido"  type="text" class="form-control" id="segundoapellido">
                </div>
            </div>

            <div class="mb-3 row">
                <label class="col-sm-2 col-form-label">
                    Genero
                </label>
                <div class="col-sm-10">      
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="genero" id="generoHombre" checked value="Hombre">
                        <label class="form-check-label" for="generoHombre">
                            Hombre
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="genero" id="generoMujer" value="Mujer">
                        <label class="form-check-label" for="generoMujer">
                        Mujer
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="genero" id="generoOtro" value="Otro">
                        <label class="form-check-label" for="generoOtro">
                        Otro
                        </label>
                    </div>
                </div>
            </div>

            <div class="mb-3 row">
                <label for="dateinput" class="col-sm-2 col-form-label">Fecha de Nacimiento</label>
                <div class="col-sm-10">
                <input type="date" class="form-control" id="dateinput" name="fecha" required>
                </div>
            </div>
        
            <div class="mb-3 row">
                <label for="contrasena" class="col-sm-2 col-form-label">
                    Contraseña
                </label>
                <div class="col-sm-10">
                <input name="pass" placeholder="Contraseña" required  type="text" class="form-control" id="Contraseña">
                </div>
        </div>




            <button type="sumbit" class="btn btn-primary">Registrar</button>
            </form>
       
        </div>
        </div>
    </div>
	
    </body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>










</html>