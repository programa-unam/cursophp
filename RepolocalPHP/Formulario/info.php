<?php session_start(); 
 $autenticado = $_SESSION['userActivo']['autenticado'];
 if($autenticado == 0){
  $msg = urlencode('Nesecitas estar logueado para acceder');
  header('Location:./login.php?msg='.$msg);
  die;
 }

?>
<html>
<head>
    <title>Info</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
      span.card-header.link-header-form {
         background: #0d6efd;
        color: white;
        }
        span.card-header.link-header-form a {
            color: #ffffff8c;
            text-decoration: none;
            padding: 5px 15px 5px 5px;
        }
        a.active {
            color: white !important;
        } 
      .container.cont-info {
        padding: 10px 80px;
      } 
      ul {
          list-style: none;
      }
      li {
        margin-left: -32px;
      }
    </style>

</head>

<body>
  <div class="container cont-info">
    <div class="card">
      <span class="card-header link-header-form">
        <a href="./info.php"class="active">Home</a>
        <a href="./formulario.php" >Registrar Alumnos</a>
        <a href="./cerrar_sesion.php">Cerrar sesión</a>
      </span>
      <div class="card-body">
      <?php if(isset($_GET['msg'])){ ?>
                                <div class="alerta">
                                    <div class="alert alert-warning" role="alert">
                                        <?php echo $_GET['msg']; ?>
                                    </div>
                                </div>
                            <?php } ?>      
      <?php if(isset($_GET['msgs'])){ ?>
          <div class="alerta">
              <div class="alert alert-warning" role="alert">
                  <?php echo $_GET['msgs']; ?>
              </div>
          </div>
      <?php } ?>                           
        <!-- usuario autenticado -->
        <div class="row" style='margin-top:10px;'>
          <h5 class="tittle-usuario">Usuario Autenticado</h5>
        </div>
        <br>
        <div class="card">
          <span class="card-header">
            <span class="tittle-usuario">
              <?php echo $_SESSION['userActivo']['nombre'].' '.$_SESSION['userActivo']['primer_apellido'] ?>
            </span>
          </span>
          <div class="card-body">
            <h6><strong>Información</strong></h6>
            <ul>
              <li><strong>Número de Cuenta: </strong><?php echo $_SESSION['userActivo']['num_cuenta']; ?></li>
              <li><strong>Fecha de Nacimiento: </strong><?php 
              $date = date_create( $_SESSION['userActivo']['fecha_nacimiento']);
              echo date_format($date, 'd/m/Y');
              ?>
              </li>
            </ul>
          </div>
        </div>

        <!-- Datos guardados -->
         <!-- usuario autenticado -->
         <div class="row" style='margin-top:20px; margin-button:-10px;'>
          <h5 class="tittle-usuario">Datos Guardados</h5>
        </div>
        <br>
        <div class="card">
          <div class="card-body">
          <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col" type="date">Fecha de Nacimiento</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if(!empty($_SESSION['Usuarios'])){
                foreach($_SESSION['Usuarios'] as $usuario => $value){ ?>
                  
                  <tr>
                    <th scope="row"><?php echo  $usuario; ?></th>
                    <td>
                    <?php 
                       echo $value['nombre'].' '.$value['primer_apellido'];
                       ?>
                    </td>
                    <td>
                      <?php 
                       $date = date_create( $value['fecha_nacimiento']);
                       echo date_format($date, 'd/m/Y');
                      ?>
                    </td>
                  </tr>
              <?php  }
              }
              ?>
            </tbody>
          </table>
          </div>
        </div>


      </div>
    </div>
  </div>
</body>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</html>
